package com.scotty.scottysalarm;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;

import java.io.IOException;


public class AlarmActivity extends Activity {

    private MediaPlayer mPlayer = new MediaPlayer();
    private AlertDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        mDialog = new AlertDialog.Builder(this)
                .setTitle("Sunrise!")
                .setMessage("It's Time to get up!")
                .setNeutralButton("Okay!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface d, int arg1) {
                        finishAndRemoveTask();
                    }
                })
                .setCancelable(false)
                .create();

        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String ringtone = preferences.getString("sunrise_ringtone_preference", RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString());
            if (ringtone != null && !ringtone.isEmpty()) {
                mPlayer.setDataSource(this, Uri.parse(ringtone));
                mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mPlayer.setLooping(true);
                mPlayer.prepare();
                mPlayer.start();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        mDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("SCOTTY", "AlarmActivity paused");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("SCOTTY", "AlarmActivity resumed");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("SCOTTY", "AlarmActivity stopped");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("SCOTTY", "AlarmActivity destroyed");
        if (mDialog.isShowing())
            mDialog.cancel();

        if (mPlayer != null) {
            if (mPlayer.isPlaying())
                mPlayer.stop();
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        Log.d("SCOTTY", "AlarmActivity onUserInteraction");
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        Log.d("SCOTTY", "AlarmActivity onUserLeaveHint");
        finishAndRemoveTask();
    }
}
