package com.scotty.scottysalarm;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Load the preferences form an XML resource
        PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, false);
        addPreferencesFromResource(R.xml.preferences);

        //Tweak the value for the custom entry in the pandora station list preference
        ListPreference listPreference = (ListPreference)findPreference("pandora_station_preference");
        CharSequence[] values = listPreference.getEntryValues();
        values[values.length - 1] = listPreference.getValue();
        listPreference.setEntryValues(values);

        updatePreferenceSummary(getPreferenceScreen());

        findPreference("sleep_duration_preference").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String value = newValue.toString();

                if (value.matches("([0-9]|0[0-9]|1[0-9]|2[0-3]):(0|[0-5][0-9])$")) {
                    return true;
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Duration not saved")
                            .setMessage("Please enter the duration in hours and minutes (HH:MM)")
                            .setPositiveButton("Ok", null)
                            .create().show();
                    return false;
                }
            }
        });

        findPreference("pandora_station_preference").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                final ListPreference listPreference = (ListPreference)preference;
                String newValue = (String)o;
                Log.d("SCOTTY", "Current Entry: " + listPreference.getEntry());

                if(newValue.equals(listPreference.getEntryValues()[listPreference.getEntryValues().length - 1])){
                    final String oldValue = listPreference.getValue();
                    Log.d("SCOTTY", "Old: " + oldValue + ", New: " + newValue);

                    final EditText newStation = new EditText(getActivity());
                    newStation.setText(oldValue);
                    newStation.setInputType(InputType.TYPE_CLASS_NUMBER);
                    newStation.setSelection(oldValue.length());

                    final AlertDialog dlg = new AlertDialog.Builder(getActivity())
                            .setTitle("Custom Pandora Station")
                            .setMessage("Enter the station id that can be found in the URL of the pandora station you wish to use")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    CharSequence[] values = listPreference.getEntryValues();
                                    values[values.length - 1] = newStation.getText().toString();
                                    listPreference.setEntryValues(values);
                                    listPreference.setValueIndex(values.length - 1);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    listPreference.setValue(oldValue);
                                }
                            })
                            .setNeutralButton("Test", null)
                            .setView(newStation)
                            .create();

                    dlg.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialog) {
                             dlg.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String pandoraStation = newStation.getText().toString();
                                    Intent pandoraIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.pandora.android");
                                    pandoraIntent.setData(Uri.parse("pandorav2:/createStation?stationId=" + pandoraStation));
                                    pandoraIntent.setFlags(pandoraIntent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    getActivity().startActivity(pandoraIntent);
                                }
                            });
                        }
                    });

                    dlg.show();
                }
                return true;
            }
        });

        findPreference("sleep_duration_preference").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                EditTextPreference editTextPreference = (EditTextPreference) preference;
                editTextPreference.getEditText().setSelection(editTextPreference.getText().length());
                return true;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    private void updatePreferenceSummary(Preference preference) {
        if (preference instanceof PreferenceGroup) {
            PreferenceGroup preferenceGroup = (PreferenceGroup) preference;
            for (int i = 0; i < preferenceGroup.getPreferenceCount(); i++) {
                updatePreferenceSummary(preferenceGroup.getPreference(i));
            }
        } else {
            if (preference instanceof EditTextPreference) {
                preference.setSummary(((EditTextPreference) preference).getText());
            } else if (preference instanceof ListPreference) {
                preference.setSummary(((ListPreference) preference).getEntry());
            } else if (preference instanceof RingtonePreference) {
                Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), Uri.parse(
                        preference.getSharedPreferences().getString(preference.getKey(), "")));
                preference.setSummary(ringtone.getTitle(getActivity()));
                ringtone.stop();
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        boolean alarmEnabled = sharedPreferences.getBoolean(getString(R.string.alarm_enabled_preference), false);

        Preference preference = findPreference(key);
        updatePreferenceSummary(preference);

        Intent disableIntent = new Intent(getActivity(), ETBBroadcastReceiver.class);
        disableIntent.setAction(ETBBroadcastReceiver.ACTION_DISABLE);

        Intent enableIntent = new Intent(getActivity(), ETBBroadcastReceiver.class);
        enableIntent.setAction(ETBBroadcastReceiver.ACTION_ENABLE);

        switch (key) {
            case "sleep_duration_preference":
                //getActivity().sendBroadcast(disableIntent);
                getActivity().sendBroadcast(enableIntent);
                break;
            case "alarm_enabled_preference":
                if (alarmEnabled) {
                    PackageManager packageManager = getActivity().getPackageManager();
                    ComponentName componentName = new ComponentName(getActivity(), ETBBroadcastReceiver.class);
                    packageManager.setComponentEnabledSetting(componentName,
                            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                            PackageManager.DONT_KILL_APP);

                    getActivity().sendBroadcast(enableIntent);
                } else {
                    getActivity().sendBroadcast(disableIntent);
                    PackageManager packageManager = getActivity().getPackageManager();
                    ComponentName componentName = new ComponentName(getActivity(), ETBBroadcastReceiver.class);
                    packageManager.setComponentEnabledSetting(componentName,
                            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                            PackageManager.DONT_KILL_APP);
                }
                break;
        }


    }
}
