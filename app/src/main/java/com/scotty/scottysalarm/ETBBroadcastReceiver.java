package com.scotty.scottysalarm;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;

import java.util.Calendar;
import java.util.TimeZone;

public class ETBBroadcastReceiver extends android.content.BroadcastReceiver implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String ACTION_ENABLE = "com.scotty.scottysalarm.action.enable";
    public static final String ACTION_DISABLE = "com.scotty.scottysalarm.action.disable";
    private static final String ACTION_SUNRISE = "com.scotty.scottysalarm.action.sunrise";
    private static final String ACTION_SUNSET = "com.scotty.scottysalarm.action.sunset";
    private static final String ACTION_BEDTIME = "com.scotty.scottysalarm.action.bedtime";
    private static final String ACTION_LOCATION_UPDATE = "com.scotty.scottysalarm.action.location_update";
    private PendingIntent mSunriseIntent;
    private PendingIntent mSunsetIntent;
    private PendingIntent mBedtimeIntent;
    private PendingIntent mLocationUpdateIntent;
    private boolean mRequestLocationUpdates;
    private GoogleApiClient mGoogleApiClient;
    private AlarmManager mAlarmManager;
    private Context mContext;

    public ETBBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent != null) {

            mContext = context;

            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            mAlarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

            Intent sunriseIntent = new Intent(context, ETBBroadcastReceiver.class);
            sunriseIntent.setAction(ACTION_SUNRISE);
            mSunriseIntent = PendingIntent.getBroadcast(context, 0, sunriseIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent sunsetIntent = new Intent(context, ETBBroadcastReceiver.class);
            sunsetIntent.setAction(ACTION_SUNSET);
            mSunsetIntent = PendingIntent.getBroadcast(context, 0, sunsetIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent bedtimeIntent = new Intent(context, ETBBroadcastReceiver.class);
            bedtimeIntent.setAction(ACTION_BEDTIME);
            mBedtimeIntent = PendingIntent.getBroadcast(context, 0, bedtimeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent locationUpdateIntent = new Intent(mContext, ETBBroadcastReceiver.class);
            locationUpdateIntent.setAction(ACTION_LOCATION_UPDATE);
            mLocationUpdateIntent = PendingIntent.getBroadcast(context, 0, locationUpdateIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            final String action = intent.getAction();
            Log.d("SCOTTY", "Received Broadcast: " + action);
            switch (action) {
                case Intent.ACTION_BOOT_COMPLETED:
                case ACTION_ENABLE:
                    mRequestLocationUpdates = true;
                    mGoogleApiClient.connect();
                    break;
                case ACTION_DISABLE:
                    handleActionDisable();
                    break;
                case ACTION_SUNRISE:
                    handleActionSunrise();
                    break;
                case ACTION_SUNSET:
                    break;
                case ACTION_BEDTIME:
                    handleActionBedtime();
                    break;
                case ACTION_LOCATION_UPDATE:
                    if (intent.hasExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED)) {
                        Location location = intent.getParcelableExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED);
                        setAlarms(location);
                    } else
                        Log.d("SCOTTY", "onReceive ACTION_LOCATION_UPDATE: No location extra found...");
                    break;
            }
        }
    }

    private void handleActionDisable() {
        if (mSunriseIntent != null) {
            mAlarmManager.cancel(mSunriseIntent);
            mSunriseIntent.cancel();
        } else Log.d("SCOTTY", "handleActionDisable: Sunrise Alarm not set");

        if (mBedtimeIntent != null) {
            mAlarmManager.cancel(mBedtimeIntent);
            mBedtimeIntent.cancel();
        } else Log.d("SCOTTY", "handleActionDisable: Bedtime Alarm not set");

        if (mSunsetIntent != null) {
            mAlarmManager.cancel(mSunsetIntent);
            mSunsetIntent.cancel();
        } else Log.d("SCOTTY", "handleActionDisable: Sunset Alarm not set");

        Log.d("SCOTTY", "handleActionDisable: Requesting location update removal");
        mRequestLocationUpdates = false;
        mGoogleApiClient.connect();

    }

    private void handleActionSunrise() {

        AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

        boolean pandoraEnabled = PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean("sunrise_pandora_enabled_preference", false);
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (pandoraEnabled && networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            int maxAlarmVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM);
            int maxMusicVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            int currentAlarmVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);

            int currentMusicVolume = (currentAlarmVolume * maxMusicVolume) / maxAlarmVolume;

            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentMusicVolume, 0);

            String pandoraStation = PreferenceManager.getDefaultSharedPreferences(mContext).getString(mContext.getString(R.string.pandora_station_preference), "");
            Intent pandoraIntent = mContext.getPackageManager().getLaunchIntentForPackage("com.pandora.android");
            //pandoraIntent.setAction(Intent.ACTION_VIEW);
            //pandoraIntent.setClassName("com.pandora.android", "com.pandora.android.Main");
            pandoraIntent.setData(Uri.parse("pandorav2:/createStation?stationId=" + pandoraStation));
            pandoraIntent.setFlags(pandoraIntent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(pandoraIntent);
        } else {
            Intent alarmIntent = new Intent(mContext, AlarmActivity.class);
            alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(alarmIntent);
        }
    }


    private void handleActionBedtime() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        boolean vibrate = preferences.getBoolean(mContext.getString(R.string.bedtime_vibrate_preference), true);

        String ringtone = preferences.getString(mContext.getString(R.string.bedtime_ringtone_preference), RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString());

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder notificationBuilder = new Notification.Builder(mContext)
                .setContentTitle("Bedtime!")
                .setContentText("Lights out if you want to wake up at sunrise!")
                .setSmallIcon(R.drawable.ic_stat_action_alarm)
                .setContentIntent(PendingIntent.getActivity(mContext, 1, new Intent(mContext, MainActivity.class), 0))
                .setSound(Uri.parse(ringtone))
                .setWhen(System.currentTimeMillis())
                .setShowWhen(true)
                .setAutoCancel(true);

        if (vibrate)
            notificationBuilder.setVibrate(new long[]{0, 750, 750, 750, 750, 750});

        notificationManager.notify(0, notificationBuilder.build());

        Runnable task = new Runnable() {
            public void run() {
                AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
                audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(task, 750 * 5);
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mRequestLocationUpdates) {
//            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            Log.d("SCOTTY", "onConnected: Requesting Location Updates");
            LocationRequest locationRequest = new LocationRequest()
                    .setPriority(LocationRequest.PRIORITY_NO_POWER);


            Intent locationUpdateIntent = new Intent(mContext, ETBBroadcastReceiver.class);
            locationUpdateIntent.setAction(ACTION_LOCATION_UPDATE);

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    locationRequest,
                    mLocationUpdateIntent);

        } else if (!mRequestLocationUpdates) {
            Log.d("SCOTTY", "onConnected: Removing location updates");
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationUpdateIntent);
        }
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("SCOTTY", "Google API Client Connection Suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("SCOTTY", "Google API Client Connection Failed");
    }

    private void setAlarms(Location currentLocation) {
        com.luckycatlabs.sunrisesunset.dto.Location location = new com.luckycatlabs.sunrisesunset.dto.Location(currentLocation.getLatitude(), currentLocation.getLongitude());

        SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, TimeZone.getDefault());
        Calendar sunrise = Calendar.getInstance();
        sunrise = calculator.getCivilSunriseCalendarForDate(sunrise);

        if (sunrise.before(Calendar.getInstance())) {
            sunrise.add(Calendar.DATE, 1);
            sunrise = calculator.getCivilSunriseCalendarForDate(sunrise);
        }

        //DEBUG
//        sunrise = Calendar.getInstance();
//        sunrise.add(Calendar.MINUTE, 1);
        //END DEBUG

        PendingIntent editIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext, MainActivity.class), PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager.AlarmClockInfo sunriseAlarmInfo = new AlarmManager.AlarmClockInfo(sunrise.getTimeInMillis(), editIntent);
        mAlarmManager.setAlarmClock(sunriseAlarmInfo, mSunriseIntent);

        Log.d("SCOTTY", "Wakeup set for: " + sunrise.getTime().toString());

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String preference = sharedPreferences.getString("sleep_duration_preference", "09:00");

        int hours = 9, minutes = 0;
        if (preference != null) {
            hours = Integer.parseInt(preference.split(":")[0]) * -1;
            minutes = Integer.parseInt(preference.split(":")[1]) * -1;
        }

        //DEBUG
//        hours = 0;
//        minutes = 0;
//        sunrise.add(Calendar.SECOND, -30);
        //END DEBUG

        sunrise.add(Calendar.HOUR, hours);//Sleep
        sunrise.add(Calendar.MINUTE, minutes);

        if (Calendar.getInstance().before(sunrise)) {
            //AlarmManager.AlarmClockInfo bedtimeAlarmInfo = new AlarmManager.AlarmClockInfo(sunrise.getTimeInMillis(), editIntent);
            //mAlarmManager.setAlarmClock(bedtimeAlarmInfo, mBedtimeIntent);
            mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, sunrise.getTimeInMillis(), mBedtimeIntent);
            Log.d("SCOTTY", "Bedtime set for: " + sunrise.getTime().toString());
        }
    }
}
